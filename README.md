# gstems-measurements
main: [![pipeline status](https://gitlab.com/growingstems/gstems-measurements/badges/main/pipeline.svg)](https://gitlab.com/growingstems/gstems-measurements/-/commits/main)<br>
[JavaDoc Here!](https://growingstems.gitlab.io/gstems-measurements/)<br><br>
Library containing source code that will generate compile time type safe representations of physical units.<br>
Uses [Spotless](https://github.com/diffplug/spotless) for code formatting. See the [Spotless Gradle](vscode:extension/richardwillis.vscode-spotless-gradle) extension for auto-formatting in VS Code!<br>

## Building
This project requires Python 3.x to build. Depending on your system, you may need to modify the `pythonBuild` task in `build.gradle` for Gradle to successfully run the Python-based scripts.<br>
