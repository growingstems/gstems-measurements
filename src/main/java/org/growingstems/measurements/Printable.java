package org.growingstems.measurements;

public interface Printable {
    String getName();

    String getPrettyName();

    String getAbbreviation();

    String getSymbol();

    default String toString(PrintStyle printStyle) {
        switch (printStyle) {
            case STANDARD:
                return getName();
            case PRETTY:
                return getPrettyName();
            case ABBREVIATION:
                return getAbbreviation();
            case SYMBOL:
                return getSymbol();
        }
        // If this is accessible, the PrintStyle enum has changed.
        // switch expressions would fix this issue, but are not available in Java 8.
        return null;
    }
}
