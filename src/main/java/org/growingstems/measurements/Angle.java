/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.measurements.Measurements.AngleImpl;

/** Implementation of the {@link Unit} type for Angle units */
public class Angle extends AngleImpl {
    /**
     * Constructs in the SI Base Unit. <br>
     *
     * @param value Unit value in the SI Base Unit
     */
    public Angle(double value) {
        super(value);
    }

    /** Constructs a unit with an absolute value of 0.0 */
    public Angle() {
        super(0.0, Unit.RADIANS);
    }

    /**
     * Constructs an Angle of a specific unit
     *
     * @param value Angle value
     * @param unit Value's unit
     */
    public Angle(double value, Unit unit) {
        super(value, unit);
    }

    /**
     * Calculates the sine of the Angle
     *
     * @return The sine of the Angle
     */
    public double sin() {
        return Math.sin(getBaseValue());
    }

    /**
     * Calculates the cosine of the Angle
     *
     * @return The cosine of the Angle
     */
    public double cos() {
        return Math.cos(getBaseValue());
    }

    /**
     * Calculates the tangent of the Angle
     *
     * @return The tangent of the Angle
     */
    public double tan() {
        return Math.tan(getBaseValue());
    }

    /**
     * Constructs an Angle equivalent to the arc-sine of the provided ratio
     *
     * @param ratio The value to perform arc-sine on, e.g. "Opposite over Hypotenus"
     * @return The Angle representing the arc-sine of the ratio
     */
    public static Angle asin(double ratio) {
        return Angle.radians(Math.asin(ratio));
    }

    /**
     * Constructs an Angle equivalent to the arc-cosine of the provided ratio
     *
     * @param ratio The value to perform arc-cosine on, e.g. "Adjacent over Hypotenus"
     * @return The Angle representing the arc-cosine of the ratio
     */
    public static Angle acos(double ratio) {
        return Angle.radians(Math.acos(ratio));
    }

    /**
     * Constructs an Angle equivalent to the arc-tangent of the provided ratio
     *
     * @param ratio The value to perform arc-tangent on, e.g. "Opposite over Adjacent"
     * @return The Angle representing the arc-tangent of the ratio
     */
    public static Angle atan(double ratio) {
        return Angle.radians(Math.atan(ratio));
    }

    /**
     * Constructs an Angle using the standard atan2 method
     *
     * @param y The vertical component to perform the atan2 operation on, e.g. "Opposite"
     * @param x The horizontal component to perform the atan2 operation on, e.g. "Adjacent"
     * @return The Angle result of the atan2 operation
     * @see <a href="https://en.wikipedia.org/wiki/Atan2" target="_top">Wikipedia: atan2</a>
     */
    public static Angle atan2(double y, double x) {
        return Angle.radians(Math.atan2(y, x));
    }

    /**
     * Get the absolute difference from another angle. <br>
     * The final Angle will be equivalent to the result of {@link #sub(Angle)}, except the Angle's
     * value is guaranteed to be on the range (-pi, pi].
     *
     * @param other The other Angle to be used in the difference operation.
     * @return a new resulting Angle from the difference of the two Angle objects
     */
    public Angle difference(Angle other) {
        // Modulus may return a negative value, but the magnitude will be less than k_twoPi.
        // The result of the modulus will therefore be on the range (-2*pi, 2*pi)
        Angle angle = Angle.radians((getBaseValue() - other.getBaseValue()) % TWO_PI.getBaseValue());
        // Move the angle range to (-pi, pi]
        if (angle.gt(PI)) {
            angle = angle.sub(TWO_PI);
        } else if (angle.le(PI.neg())) {
            angle = angle.add(TWO_PI);
        }
        return angle;
    }

    /**
     * Shifts the current angle to the correct scope of a reference angle. Used to shift the current
     * angle such that it will be the shortest distance to a provided reference angle. An example of
     * use is with a Swerve Module that is kept at a steer angle that is continuous from (-inf, inf),
     * and is being driven by an angle from [wrappedLow, wrappedHigh].
     *
     * @param referenceAngle Angle that the wound up angle's scope is based off of.
     * @param wrappedLow The lower bound of referenceAngle.
     * @param wrappedHigh The upper bound of referenceAngle.
     * @return Wound up angle that is within the scope of referenceANgle.
     */
    public Angle shiftWrappedAngleScope(Angle referenceAngle, Angle wrappedLow, Angle wrappedHigh) {
        Angle wrappedRange = wrappedHigh.sub(wrappedLow);

        // First offset the angle to simplify math.
        Angle adjustedRef = referenceAngle.sub(wrappedLow);
        Angle referenceScope = adjustedRef.floor(Unit.REVOLUTIONS);

        // Refers to raw value
        Angle shiftedAngle = this.add(referenceScope);
        Angle delta = referenceAngle.sub(shiftedAngle);
        Angle wrappedRangeOverTwo = wrappedRange.div(2);

        if (delta.le(wrappedRangeOverTwo) && delta.ge(wrappedRangeOverTwo.neg())) {
            return shiftedAngle;
        } else if (delta.gt(wrappedRangeOverTwo)) {
            return shiftedAngle.add(wrappedRange);
        } else {
            return shiftedAngle.sub(wrappedRange);
        }
    }

    /**
     * Shifts a bounded angle to the correct scope of a unbounded angle. Used to shift a bounded angle
     * such that it will be the shortest distance to the reference angle. An example of use is with a
     * Swerve Module that is kept at a steer angle that is continuous from (-inf, inf), and is being
     * driven by an angle from (-Pi, Pi].
     *
     * @param referenceAngle Angle that the new angle's scope is based off of.
     * @return Wound up angle that is within the scope of the reference angle.
     */
    public Angle shiftWrappedAngleScope(Angle referenceAngle) {
        return difference(referenceAngle).add(referenceAngle);
    }
}
