package org.growingstems.measurements;

public enum PrintStyle {
    STANDARD,
    PRETTY,
    ABBREVIATION,
    SYMBOL
}
