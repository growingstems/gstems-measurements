/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import org.growingstems.measurements.Measurements.DivByUnitless;
import org.growingstems.measurements.Measurements.MulByUnitless;
import org.growingstems.measurements.Measurements.Unitless;

/**
 * Defines what functions all Units must implement.
 *
 * <p>Intended to allow for compile-time safety when performing math with unit-based values.
 *
 * @param <T> The class implementing this interface. Used to provide knowledge to the generic
 *     interfaces.
 */
public interface Unit<T extends Unit<T>> extends MulByUnitless<T>, DivByUnitless<T> {
    /**
     * Gets the Base value of the Unit. <br>
     *
     * @return Value in the SI Base unit
     */
    double getBaseValue();

    /**
     * Adds two T objects together, one being the original and the other being the argument.
     *
     * @param other The other T to be used in the addition operation.
     * @return a new resulting T from the summation of the two T objects.
     */
    T add(T other);

    /**
     * Subtracts another T from the current object. <br>
     * <br>
     * The current object is the minuend, and the other object is the subtrahend.
     *
     * @param other The subtrahend in the subtraction operation
     * @return The difference between the two objects
     */
    T sub(T other);

    /**
     * Multiplies T by the given scalar value.
     *
     * @param s The scalar value
     * @return a new T after multiplying by the scalar value
     */
    T mul(double s);

    /**
     * Divides T by the given scalar value.
     *
     * @param s The scalar value
     * @return a new T after dividing by the scalar value
     */
    T div(double s);

    /**
     * Divides T by another T.
     *
     * @param t The other T
     * @return a Unitless after dividing by the other T
     */
    Unitless div(T t);

    /**
     * Takes the absolute value of the unit.
     *
     * @return a new T that is the abolute value of this
     */
    T abs();

    /**
     * Negates the unit.
     *
     * @return a new T that is the negative value of this
     */
    T neg();

    /**
     * Applies a power to the unit.
     *
     * @param s The scalar value
     * @return a new T after raising it to the power of the scalar value
     */
    T pow(double s);

    /**
     * Finds the square root of the unit.
     *
     * @return a new T that is the square root of this
     */
    T sqrt();

    /**
     * Compares this with another T for equality
     *
     * @param rhs the value to compare this to
     * @return true if this and rhs are equal
     */
    boolean eq(T rhs);

    /**
     * Compares this with another T for inequality
     *
     * @param rhs the value to compare this to
     * @return true if this and rhs are not equal
     */
    boolean ne(T rhs);

    /**
     * Checks if this is greater than the passed in T
     *
     * @param rhs the value to compare this to
     * @return true if this is greater than rhs
     */
    boolean gt(T rhs);

    /**
     * Checks if this is greater than or equal to the passed in T
     *
     * @param rhs the value to compare this to
     * @return true if this is greater than or equal to rhs
     */
    boolean ge(T rhs);

    /**
     * Checks if this is less than the passed in T
     *
     * @param rhs the value to compare this to
     * @return true if this is less than rhs
     */
    boolean lt(T rhs);

    /**
     * Checks if this is less than or equal to the passed in T
     *
     * @param rhs the value to compare this to
     * @return true if this is less than or equal to rhs
     */
    boolean le(T rhs);

    /**
     * Uses {@link Double#compare(double, double)} to compare the base values
     *
     * @param rhs the T to be compared
     * @return The result of {@link Double#compare(double, double)}
     */
    int compareTo(T rhs);

    /**
     * Uses {@link Double#isFinite(double)} on the base value
     *
     * @return The result of {@link Double#isFinite(double)}
     */
    boolean isFinite();

    /**
     * Uses {@link Double#isInfinite(double)} on the base value
     *
     * @return The result of {@link Double#isInfinite(double)}
     */
    boolean isInfinite();

    /**
     * Uses {@link Double#isNaN(double)} on the base value
     *
     * @return The result of {@link Double#isNaN(double)}
     */
    boolean isNaN();

    /**
     * Uses {@link Unit#compareTo(Unit)} on the first T, providing the second T
     *
     * @param <T> The type of Unit being compared
     * @param lhs The first T to compare
     * @param rhs The second T to compare
     * @return The result of {@link Unit#compareTo(Unit)}
     */
    static <T extends Unit<T>> int compare(T lhs, T rhs) {
        return lhs.compareTo(rhs);
    }

    /**
     * Uses {@link Unit#isFinite()} on the value
     *
     * @param <T> The type of Unit being checked
     * @param value The value being checked
     * @return The result of {@link Unit#isFinite()}
     */
    static <T extends Unit<T>> boolean isFinite(T value) {
        return value.isFinite();
    }

    /**
     * Uses {@link Unit#isInfinite()} on the value
     *
     * @param <T> The type of Unit being checked
     * @param value The value being checked
     * @return The result of {@link Unit#isInfinite()}
     */
    static <T extends Unit<T>> boolean isInfinite(T value) {
        return value.isInfinite();
    }

    /**
     * Uses {@link Unit#isNaN()} on the value
     *
     * @param <T> The type of Unit being checked
     * @param value The value being checked
     * @return The result of {@link Unit#isNaN()}
     */
    static <T extends Unit<T>> boolean isNaN(T value) {
        return value.isNaN();
    }

    /**
     * Returns the greater of the two T values. <br>
     * <br>
     * If either value is NaN, then the result is NaN, similar to {@link Math#max(double, double)}.
     *
     * @param <T> The type of Unit being compared
     * @param lhs The first T to compare
     * @param rhs The second T to compare
     * @return The greater of the two values
     */
    static <T extends Unit<T>> T max(T lhs, T rhs) {
        return lhs.isNaN() ? lhs : (lhs.gt(rhs) ? lhs : rhs);
    }

    /**
     * Returns the lesser of the two T values. <br>
     * <br>
     * If either value is NaN, then the result is NaN, similar to {@link Math#min(double, double)}.
     *
     * @param <T> The type of Unit being compared
     * @param lhs The first T to compare
     * @param rhs The second T to compare
     * @return The lesser of the two values
     */
    static <T extends Unit<T>> T min(T lhs, T rhs) {
        return lhs.isNaN() ? lhs : (lhs.lt(rhs) ? lhs : rhs);
    }

    /**
     * Adds two Unit values using {@link Unit#add(Unit)}. <br>
     * <br>
     * Exists solely to closer match the {@link Double} class.
     *
     * @param <T> The type of Unit being added
     * @param lhs The first T to sum
     * @param rhs The second T to sum
     * @return the sum of the two values
     */
    static <T extends Unit<T>> T sum(T lhs, T rhs) {
        return lhs.add(rhs);
    }
}
