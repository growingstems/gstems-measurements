# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from Types import UnitType, Dimensionality, Constant, Unit, to_camel_lower, to_camel_upper
from typing import TextIO


def get_header(package: str):
    return f"""
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package {package};

import java.util.Objects;

/**
 * Container class which has all units which don't have \"extra functions\" <br>
 *
 * Extra functions are functions which not all units have. For example, {{@link {package}.Angle#difference}}
 */
public class Measurements {{
""".lstrip('\n')

def get_footer():
    return """
}
""".lstrip('\n')

def get_type_header(type_name: str, class_name: str, implementations: list[str]):
    return f"""
    /**
     * Implementation of the {{@link Unit}} type for {type_name} units
     */
    public static class {class_name} implements {','.join(implementations)} {{
""".lstrip('\n')

def get_type_standard_functions(type_name: str, class_name: str, default_unit: Unit, has_impl: bool):
    constructor_access = 'protected' if has_impl else 'public'
    return f"""
        //intentional package private
        final double m_val;

        @Override
        public double getBaseValue() {{
            return m_val;
        }}

        @Override
        public int compareTo({type_name} rhs) {{
            return Double.compare(m_val, rhs.m_val);
        }}

        @Override
        public boolean isFinite() {{
            return Double.isFinite(m_val);
        }}

        @Override
        public boolean isInfinite() {{
            return Double.isInfinite(m_val);
        }}

        @Override
        public boolean isNaN() {{
            return Double.isNaN(m_val);
        }}

        /**
         * Constructs in the SI Base Unit. <br>
         *
         * @param value Unit value in the SI Base Unit
         */
        {constructor_access} {class_name}(double value) {{
            m_val = value;
        }}

        /**
         * Constructs a unit with an absolute value of 0.0
         */
        {constructor_access} {class_name}() {{
            this(0.0, Unit.{default_unit.name});
        }}

        /**
         * Constructs a {type_name} of a specific unit
         *
         * @param value {type_name} value
         * @param unit Value's unit
         */
        {constructor_access} {class_name}(double value, Unit unit) {{
            m_val = (value - unit.getOffset()) * unit.getScale();
        }}

        /**
         * Calls {{@link #toString(Unit)}} with the default unit
         *
         * @return String representation of the {type_name}
         */
        @Override
        public String toString() {{
            return toString(Unit.{default_unit.name});
        }}

        /**
         * Creates a string representing the unit. <br>
         *
         * The unit will be represented by its value followed by the unit of the value.
         *
         * @param unit The unit in which to represent the value
         * @return String representation of the {type_name}
         */
        public String toString(Unit unit) {{
            return getValue(unit) + " " + unit.toString();
        }}

        /**
         * Obtain a numeric value of the unit in a specified unit
         *
         * @param unit The unit in which to retrieve a value
         * @return The value in the specified unit
         */
        public double getValue(Unit unit) {{
            return (m_val / unit.getScale()) + unit.getOffset();
        }}

        @Override
        public {type_name} add({type_name} other) {{
            return new {type_name}(m_val + other.m_val);
        }}

        @Override
        public {type_name} sub({type_name} other) {{
            return new {type_name}(m_val - other.m_val);
        }}

        @Override
        public {type_name} mul(double s) {{
            return new {type_name}(m_val * s);
        }}

        @Override
        public {type_name} div(double s) {{
            return new {type_name}(m_val / s);
        }}

        @Override
        public {type_name} abs() {{
            return new {type_name}(Math.abs(m_val));
        }}

        @Override
        public {type_name} neg() {{
            return new {type_name}(-m_val);
        }}

        @Override
        public {type_name} pow(double s) {{
            return new {type_name}(Math.pow(m_val, s));
        }}

        @Override
        public {type_name} sqrt() {{
            return new {type_name}(Math.sqrt(m_val));
        }}

        @Override
        public boolean eq({type_name} rhs) {{
            return m_val == rhs.m_val;
        }}

        @Override
        public boolean ne({type_name} rhs) {{
            return m_val != rhs.m_val;
        }}

        @Override
        public boolean gt({type_name} rhs) {{
            return m_val > rhs.m_val;
        }}

        @Override
        public boolean ge({type_name} rhs) {{
            return m_val >= rhs.m_val;
        }}

        @Override
        public boolean lt({type_name} rhs) {{
            return m_val < rhs.m_val;
        }}

        @Override
        public boolean le({type_name} rhs) {{
            return m_val <= rhs.m_val;
        }}

        /**
         * Rounds to the nearest integer of a particular unit
         *
         * @param unit Unit to round to
         * @return {type_name} which can be represented by an integer in the given unit
         */
        public {type_name} round(Unit unit) {{
            return new {type_name}(Math.round(getValue(unit)), unit);
        }}

        /**
         * Rounds to the nearest integer of a particular unit
         *
         * @param unit Unit to round to
         * @return {type_name} which can be represented by an integer in the given unit
         */
        public {type_name} ceil(Unit unit) {{
            return new {type_name}(Math.ceil(getValue(unit)), unit);
        }}

        /**
         * Rounds to the nearest integer of a particular unit
         *
         * @param unit Unit to round to
         * @return {type_name} which can be represented by an integer in the given unit
         */
        public {type_name} floor(Unit unit) {{
            return new {type_name}(Math.floor(getValue(unit)), unit);
        }}

        @Override
        public boolean equals(Object obj) {{
            if (obj instanceof {type_name}) {{
                {type_name} other = ({type_name}) obj;
                return this.m_val == other.m_val;
            }}
            return false;
        }}

        @Override
        public int hashCode() {{
            return Objects.hash(m_val, "{type_name}");
        }}
""".lstrip('\n')

def get_alternate_conversion(type_name: str):
    return f"""
        /**
         * Convert to a {{@link {type_name}}}, which has the same required dimensions.
         *
         * @return the value as a {type_name}
         */
        public {type_name} as{type_name}() {{
            return new {type_name}(m_val);
        }}

""".lstrip('\n')

def get_dimension_string(dimensionality: Dimensionality):
    return '\n'.join([f"""        /** {to_camel_upper(name)} dimensionality */
        public static final int DIM_{name.upper()} = {value};""" for name, value in sorted(dimensionality.dimensions.items())]).lstrip('\n')

def get_constant_string(constant: Constant, type_name):
    return f"""
        /** {constant.description} */
        public static final {type_name} {constant.name} = {type_name}.{to_camel_lower(constant.unit)}({constant.value});
""".lstrip('\n')

def get_mul_func(rhs: UnitType, res: UnitType):
    return f"""
        @Override
        public {res.name} mul({rhs.name} val) {{
            return new {res.name}(m_val * val.getBaseValue());
        }}

""".lstrip('\n')

def get_div_func(rhs: UnitType, res: UnitType):
    return f"""
        @Override
        public {res.name} div({rhs.name} val) {{
            return new {res.name}(m_val / val.getBaseValue());
        }}

""".lstrip('\n')

def get_rec_func(res: UnitType):
    return f"""
        /**
         * Reciprocate the unit
         *
         * @return the reciprocated unit
         */
        public {res.name} rec() {{
            return new {res.name}(1.0 / m_val);
        }}

""".lstrip('\n')

def get_type_footer(type_name: str):
    return f"""
    }}

    /**
     * Defined a type which can be the multiplied by a {{@link {type_name}}}.
     *
     * @param <R> The resulting {{@link Unit}} type
     */
    public interface MulBy{type_name}<R extends Unit<R>> {{
        /**
         * Multiplies the current {{@link Unit}} by a {{@link {type_name}}}
         *
         * @param val The {type_name} value
         * @return The result of the multiplication
         */
        R mul({type_name} val);
    }}

    /**
     * Defined a type which can be the dividend with a {{@link {type_name}}} divisor.
     *
     * @param <R> The resulting {{@link Unit}} type
     */
    public interface DivBy{type_name}<R extends Unit<R>> {{
        /**
         * Divides the current {{@link Unit}} by a {{@link {type_name}}}
         *
         * @param val The {type_name} value
         * @return The result of the division
         */
        R div({type_name} val);
    }}
""".lstrip('\n')

def get_unit_header(type_name: str):
    return f"""
        /**
         * Enum representing all defined units of {type_name}
         */
        public static enum Unit implements Printable {{
""".lstrip('\n')

def get_unit_string(unit: Unit, is_last: bool):
    opt_base_unit_str = " (SI Base Unit)" if unit.scale == 1.0 else ""
    opt_offset_description_str = "" if unit.offset is None else f" + {unit.offset}"
    opt_offset_param_str = "" if unit.offset is None else f", {unit.offset}"
    final_char = ';' if is_last else ','
    return f"""
            /** {unit.pretty_name}{opt_base_unit_str}: {unit.scale} base units{opt_offset_description_str} */
            {unit.name}({unit.scale}{opt_offset_param_str}, "{unit.name}", "{unit.pretty_name}", "{unit.abbreviation}", "{unit.symbol}"){final_char}
""".lstrip('\n')

def get_unit_footer():
    return f"""
            private double m_scale;
            private double m_offset;
            private String m_name;
            private String m_prettyName;
            private String m_abbreviation;
            private String m_symbol;

            private Unit(double scale, String name, String prettyName, String abbreviation, String symbol) {{
              this(scale, 0.0, name, prettyName, abbreviation, symbol);
            }}

            private Unit(double scale, double offset, String name, String prettyName, String abbreviation, String symbol) {{
                m_scale = scale;
                m_offset = offset;
                m_name = name;
                m_prettyName = prettyName;
                m_abbreviation = abbreviation;
                m_symbol = symbol;
            }}

            private double getScale() {{
                return m_scale;
            }}

            private double getOffset() {{
              return m_offset;
            }}

            @Override
            public String getName() {{
                return m_name;
            }}

            @Override
            public String getPrettyName() {{
                return m_prettyName;
            }}

            @Override
            public String getAbbreviation() {{
                return m_abbreviation;
            }}

            @Override
            public String getSymbol() {{
                return m_symbol;
            }}
        }}

""".lstrip('\n')

def get_unit_convenience_functions(type_name: str, unit: Unit):
    unit_lowercase = unit.name.lower()
    return f"""
        /**
         * Convenience function to create a {{@link {type_name}}} in {unit_lowercase}
         *
         * @param value The value in {unit_lowercase}
         * @return A {type_name} in the corresponding unit
         */
        public static {type_name} {to_camel_lower(unit.name)}(double value) {{
            return new {type_name}(value, Unit.{unit.name});
        }}

        /**
         * Convenience function to obtain a value for the {{@link {type_name}}} in {unit_lowercase}
         *
         * @return A double in the corresponding unit
         */
        public double as{to_camel_upper(unit.name)}() {{
            return getValue(Unit.{unit.name});
        }}

""".lstrip('\n')


class JavaLibWriter:
    package: str
    types: list[UnitType]
    equal_conversion_map: dict[UnitType, set[UnitType]]
    rec_graph: dict[UnitType, tuple[UnitType, int]]
    mul_graph: dict[UnitType, dict[UnitType, tuple[UnitType, int]]]
    div_graph: dict[UnitType, dict[UnitType, tuple[UnitType, int]]]
    lines: list[str]

    def __init__(self, package: str, types: list[UnitType], equal_conversion_map: dict[UnitType, set[UnitType]], rec_graph: dict[UnitType, UnitType], mul_graph: dict[UnitType, dict[UnitType, UnitType]], div_graph: dict[UnitType, dict[UnitType, UnitType]], tests: dict[str, list[str]]):
        self.package = package
        self.types = types
        self.equal_conversion_map = equal_conversion_map
        self.rec_graph = rec_graph
        self.mul_graph = mul_graph
        self.div_graph = div_graph
        self.lines = []

    def write_to(self, f: TextIO):
        self.lines = []
        self.add_header()
        for type in self.types:
            self.add_type(type)
        self.add_footer()

        f.writelines(self.lines)

    def add_type(self, type: UnitType):
        type_name = type.name
        class_name = type_name + "Impl" if type.extra_functions else type_name
        has_impl = type.extra_functions

        default_unit = type.units[0]

        # Add Header
        implementations = ["Unit<%s>" % type_name]
        for rhs, (res, res_cost) in self.mul_graph[type].items():
            if rhs.name != "Unitless":
                implementations.append(f"\n            MulBy{rhs.name}<{res.name}>")
        for rhs, (res, res_cost) in self.div_graph[type].items():
            if rhs.name != "Unitless":
                implementations.append(f"\n            DivBy{rhs.name}<{res.name}>")
        self.lines.append(get_type_header(type_name, class_name, implementations))

        # Add dimensionality
        self.lines.append(get_dimension_string(type.required_dims))
        self.lines.append(get_dimension_string(type.optional_dims))

        # Add constants
        for constant in type.constants:
            self.lines.append(get_constant_string(constant, type_name))
        self.lines.append("\n")

        # Create Unit enum
        self.lines.append(get_unit_header(type_name))
        for unit in type.units[:-1]:
            self.lines.append(get_unit_string(unit, is_last=False))
        self.lines.append(get_unit_string(type.units[-1], is_last=True))
        self.lines.append("\n")
        self.lines.append(get_unit_footer())

        # Add convenience functions
        for unit in type.units:
            self.lines.append(get_unit_convenience_functions(type_name, unit))

        self.lines.append(get_type_standard_functions(type_name, class_name, default_unit, has_impl))
        for alternate in self.equal_conversion_map[type]:
            self.lines.append(get_alternate_conversion(alternate.name))
        for rhs, (res, res_cost) in self.mul_graph[type].items():
            self.lines.append(get_mul_func(rhs, res))
        for rhs, (res, res_cost) in self.div_graph[type].items():
            self.lines.append(get_div_func(rhs, res))
        rec = self.rec_graph.get(type)
        if rec is not None:
            rec_type, rec_cost = rec
            self.lines.append(get_rec_func(rec_type))
        self.lines.append(get_type_footer(type_name))

    def add_header(self):
        self.lines.append(get_header(self.package))

    def add_footer(self):
        self.lines.append(get_footer())
