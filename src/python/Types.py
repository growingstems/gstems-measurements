# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from __future__ import annotations

def to_pretty_name(name):
    words = ["per" if s.lower() == "per" else s.capitalize() for s in name.split("_")]
    if words:
        words[0] = words[0].capitalize()
    return ' '.join(words)

def to_camel_lower(name):
    camel_upper = to_camel_upper(name)
    return camel_upper[:1].lower() + camel_upper[1:] if camel_upper else ''

def to_camel_upper(name):
    return ''.join([s.capitalize() for s in name.lower().split("_")])

class TestData:
    add: list[str]
    subtract: list[str]
    multiply: list[str]
    divide: list[str]
    negate: list[str]
    absolute_value: list[str]
    equal_to: list[str]
    not_equal_to: list[str]
    greater_than: list[str]
    greater_than_or_equal_to: list[str]
    less_than: list[str]
    less_than_or_equal_to: list[str]
    round: list[str]
    ceil: list[str]
    floor: list[str]
    test_count: int
    test_names: set[str]

    def __init__(self, test_json: dict[str, list[str]]):
        self.test_count = 0
        self.test_names = set()
        self.add = self.assert_exists_and_count(test_json, 'add')
        self.subtract = self.assert_exists_and_count(test_json, 'subtract')
        self.multiply = self.assert_exists_and_count(test_json, 'multiply')
        self.divide = self.assert_exists_and_count(test_json, 'divide')
        self.negate = self.assert_exists_and_count(test_json, 'negate')
        self.absolute_value = self.assert_exists_and_count(test_json, 'absoluteValue')
        self.equal_to = self.assert_exists_and_count(test_json, 'equalTo')
        self.not_equal_to = self.assert_exists_and_count(test_json, 'notEqualTo')
        self.greater_than = self.assert_exists_and_count(test_json, 'greaterThan')
        self.greater_than_or_equal_to = self.assert_exists_and_count(test_json, 'greaterThanOrEqualTo')
        self.less_than = self.assert_exists_and_count(test_json, 'lessThan')
        self.less_than_or_equal_to = self.assert_exists_and_count(test_json, 'lessThanOrEqualTo')
        self.round = self.assert_exists_and_count(test_json, 'round')
        self.ceil = self.assert_exists_and_count(test_json, 'ceil')
        self.floor = self.assert_exists_and_count(test_json, 'floor')
        assert len(test_json) == self.test_count, f"Unrecognized tests: {','.join(set(test_json.keys()) - self.test_names)}"

    def assert_exists_and_count(self, test_json: dict[str, list[str]], test_name: str):
        data = test_json[test_name]
        assert data is not None, f"Missing test data for {test_name}"
        self.test_count += 1
        self.test_names.add(test_name)
        return data

class Dimensionality:
    dimensions: dict[str, int]

    def __init__(self, dimensions: dict[str, int] = {}) -> None:
        for k, v in dimensions.items():
            assert isinstance(k, str), f'{k} should be string but was {type(k)}'
            assert isinstance(v, int), f'{v} should be int but was {type(k)}'
        self.dimensions = {k: v for k, v in dimensions.items() if v != 0}

    def __eq__(self, other: Dimensionality) -> bool:
        if not isinstance(other, Dimensionality):
            return NotImplemented
        return self.dimensions == other.dimensions

    # Returns the sum of the absolute difference of every dimension
    # A zero indicates a perfect match.
    def compare_to(self, other: Dimensionality) -> int:
        cost = 0
        for dim in self.dimensions.keys() | other.dimensions.keys():
            self_value, other_value = [d.dimensions.get(dim, 0) for d in [self, other]]
            cost += abs(self_value - other_value)
        return cost

    # Reciprocate the dimensionality
    def __neg__(self) -> Dimensionality:
        return Dimensionality({k: -v for k, v in self.dimensions.items()})

    # Raising the dimensionality to a power
    def __mul__(self, other: int) -> Dimensionality:
        if not isinstance(other, int):
            return NotImplemented
        return Dimensionality({k: v * other for k, v in self.dimensions.items()})

    # Muiltiply the dimensionality by another dimensionality
    def __add__(self, other: Dimensionality) -> Dimensionality:
        if not isinstance(other, Dimensionality):
            return NotImplemented
        return Dimensionality({k: self.dimensions.get(k, 0) + other.dimensions.get(k, 0) for k in self.dimensions.keys() | other.dimensions.keys()})

    # Divide the dimensionality by another dimensionality
    def __sub__(self, other: Dimensionality) -> Dimensionality:
        if not isinstance(other, Dimensionality):
            return NotImplemented
        return Dimensionality({k: self.dimensions.get(k, 0) - other.dimensions.get(k, 0) for k in self.dimensions.keys() | other.dimensions.keys()})

    def __str__(self) -> str:
        return str(self.dimensions)

class Constant:
    name: str
    value: float
    unit: str
    description: str

    def __init__(self, json_data) -> None:
        self.name = json_data['name']
        assert isinstance(self.name, str)
        self.value = json_data['value']
        assert isinstance(self.value, float)
        self.unit = json_data['unit']
        assert isinstance(self.unit, str)
        self.description = json_data['description']
        assert isinstance(self.description, str)

    def __str__(self) -> str:
        return f"name: {self.name}, value: {self.value}, unit: {self.unit}, description: {self.description}"

class Unit:
    name: str
    scale: float
    pretty_name: str
    abbreviation: str
    symbol: str
    offset: float

    def __init__(self, json_data: dict) -> None:
        # Get Name
        self.name = json_data['name']
        assert isinstance(self.name, str)

        # Get Pretty Name
        self.pretty_name = json_data.get('pretty', to_pretty_name(self.name))
        assert isinstance(self.pretty_name, str)

        # Get Pretty-Short Name
        self.abbreviation = json_data['abbreviation']
        assert isinstance(self.abbreviation, str)

        # Get Symbol
        self.symbol = json_data.get('symbol', self.abbreviation)
        assert isinstance(self.symbol, str)

        # Get Scale and Offset
        self.scale = json_data['scale']
        assert isinstance(self.scale, float)
        if 'offset' in json_data:
            self.offset = json_data['offset']
            assert isinstance(self.offset, float)
        else:
            self.offset = None

    def __str__(self) -> str:
        return f"name: {self.name}, value: {self.scale}"

class UnitTestValue:
    unit: str
    value: float

    def __init__(self, json_data: dict) -> None:
        self.unit = json_data['unit']
        assert isinstance(self.unit, str)
        self.value = json_data['value']
        assert isinstance(self.value, float)

class UnitTestData:
    equality: list[list[UnitTestValue]]

    def __init__(self, json_data: dict) -> None:
        equality_tests = json_data.get('equality')
        if equality_tests is None:
            self.equality = []
        else:
            self.equality = [[UnitTestValue(value) for value in equality_test] for equality_test in equality_tests]

class UnitType:
    name: str
    required_dims: Dimensionality
    optional_dims: Dimensionality
    relative_dims: Dimensionality
    extra_functions: bool
    units: list[Unit]
    constants: list[Constant]
    test_data: UnitTestData

    def __init__(self, json_data) -> None:
        self.name = json_data['name']
        try:
            dim = json_data.get('dimensionality')
            match dim.get('type'):
                case 'absolute':
                    self.required_dims = Dimensionality(dim.get('required', {}))
                    self.optional_dims = Dimensionality(dim.get('optional', {}))
                    self.relative_dims = None
                case 'relative':
                    self.required_dims = None
                    self.optional_dims = None
                    self.relative_dims = Dimensionality(dim.get('compilation'))
            self.extra_functions = json_data.get('extra_functions', False)
            self.units = [Unit(j) for j in json_data['units']]
            assert self.units[0].scale == 1.0, f'Error parsing {self.name}: First unit must have a scale of 1.0!'
            self.constants = [Constant(j) for j in json_data.get('constants', [])]
            self.test_data = UnitTestData(json_data['tests'])
        except Exception:
            print(f'Caught exception while parsing {self.name}')
            raise

    def __str__(self) -> str:
        return self.name

