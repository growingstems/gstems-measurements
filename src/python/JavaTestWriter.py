# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from Types import UnitType, Dimensionality, Constant, Unit, to_pretty_name, to_camel_lower, to_camel_upper, TestData
from typing import TextIO


def get_header(package: str):
    return f"""
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package {package};

import static {package}.Measurements.*;

import static org.growingstems.utils.TestUtils.assertPercentError;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Base_Test {{
    private static final double k_tolerancePercentage = 0.1;

    public static double percentError(double expected, double actual) {{
        if (expected == 0 && actual == 0) {{
            return 0.0;
        }} else if (expected == 0) {{
            return Double.POSITIVE_INFINITY;
        }}

        return Math.abs((actual - expected) / expected) * 100.0;
    }}
""".lstrip('\n')

def get_footer():
    return """
}
""".lstrip('\n')

def get_identity_test(type: UnitType):
    return (f"""
    @Test
    public void {type.name}_identity() {{""" + ''.join([f"""
        assertPercentError(1.0, {type.name}.{to_camel_lower(unit.name)}(1.0).as{to_camel_upper(unit.name)}(), k_tolerancePercentage, "{unit.name}");""" for unit in type.units]) + f"""
    }}
""").lstrip('\n')

def get_string_test(type: UnitType, default_unit: Unit):
    return (f"""
    @Test
    public void {type.name}_string() {{
        {type.name} default{type.name} = {type.name}.{to_camel_lower(default_unit.name)}(0.0);""" + ''.join([f"""
        assertEquals("{unit.offset if unit.offset is not None else 0.0} {unit.name}", default{type.name}.toString({type.name}.Unit.{unit.name}));""" for unit in type.units]) + f"""
    }}
""").lstrip('\n')

def get_equality_tests(type: UnitType):
    return ''.join([f"""
    @Test
    public void {type.name}_equality_{idx}() {{
        {type.name} toEqual = {type.name}.{to_camel_lower(test[0].unit)}({test[0].value});""" + ''.join([f"""
        assertPercentError(toEqual, {type.name}.{to_camel_lower(comparison.unit)}({comparison.value}), k_tolerancePercentage);""" for comparison in test]) + f"""
    }}
""" for idx, test in enumerate(type.test_data.equality)]).lstrip('\n')

def get_add_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{ ', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_add(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, double expected_{default_unit.name.lower()}) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        {type.name} result = a.add(b);
        assertPercentError(expected_{default_unit.name.lower()}, result.getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_subtract_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_subtract(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, double expected_{default_unit.name.lower()}) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        {type.name} result = a.sub(b);
        assertPercentError(expected_{default_unit.name.lower()}, result.getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_multiply_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_multiply(double a_{default_unit.name.lower()}, double b_unitless, double expected_{default_unit.name.lower()}) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        Unitless b = Unitless.none(b_unitless);

        {type.name} result = a.mul(b);
        assertPercentError(expected_{default_unit.name.lower()}, result.getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_divide_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_divide(double a_{default_unit.name.lower()}, double b_unitless, double expected_{default_unit.name.lower()}) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        Unitless b = Unitless.none(b_unitless);

        {type.name} result = a.div(b);
        assertPercentError(expected_{default_unit.name.lower()}, result.getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_negate_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_negate(double initial_{default_unit.name.lower()}, double expected_{default_unit.name.lower()}) {{
        {type.name} initial{type.name} = {type.name}.{to_camel_lower(default_unit.name)}(initial_{default_unit.name.lower()});

        assertPercentError(expected_{default_unit.name.lower()}, initial{type.name}.neg().getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_absolute_value_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_absoluteValue(double initial_{default_unit.name.lower()}, double expected_{default_unit.name.lower()}) {{
        {type.name} initial{type.name} = {type.name}.{to_camel_lower(default_unit.name)}(initial_{default_unit.name.lower()});

        assertPercentError(expected_{default_unit.name.lower()}, initial{type.name}.abs().getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_equal_to_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_equalTo(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, boolean expected_equals) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        assertEquals(a.eq(b), expected_equals);
    }}
""".lstrip('\n')

def get_not_equal_to_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_notEqualTo(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, boolean expected_equals) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        assertEquals(a.ne(b), expected_equals);
    }}
""".lstrip('\n')

def get_greater_than_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_greaterThan(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, boolean expected_equals) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        assertEquals(a.gt(b), expected_equals);
    }}
""".lstrip('\n')

def get_greater_than_or_equal_to_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_greaterThanOrEqualTo(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, boolean expected_equals) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        assertEquals(a.ge(b), expected_equals);
    }}
""".lstrip('\n')

def get_less_than_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_lessThan(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, boolean expected_equals) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        assertEquals(a.lt(b), expected_equals);
    }}
""".lstrip('\n')

def get_less_than_or_equal_to_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_lessThanOrEqualTo(double a_{default_unit.name.lower()}, double b_{default_unit.name.lower()}, boolean expected_equals) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(a_{default_unit.name.lower()});
        {type.name} b = {type.name}.{to_camel_lower(default_unit.name)}(b_{default_unit.name.lower()});

        assertEquals(a.le(b), expected_equals);
    }}
""".lstrip('\n')

def get_round_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_round(double initial_{default_unit.name.lower()}, double expected_{default_unit.name.lower()}) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(initial_{default_unit.name.lower()});

        assertPercentError(expected_{default_unit.name.lower()}, a.round({type.name}.Unit.{default_unit.name}).getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_ceil_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_ceil(double initial_{default_unit.name.lower()}, double expected_{default_unit.name.lower()}) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(initial_{default_unit.name.lower()});

        assertPercentError(expected_{default_unit.name.lower()}, a.ceil({type.name}.Unit.{default_unit.name}).getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')

def get_floor_test(type: UnitType, default_unit: Unit, data: list[str]):
    return f"""
    @ParameterizedTest
    @CsvSource({{{', '.join(['"' + datum + '"' for datum in data])}}})
    public void {type.name}_floor(double initial_{default_unit.name.lower()}, double expected_{default_unit.name.lower()}) {{
        {type.name} a = {type.name}.{to_camel_lower(default_unit.name)}(initial_{default_unit.name.lower()});

        assertPercentError(expected_{default_unit.name.lower()}, a.floor({type.name}.Unit.{default_unit.name}).getValue({type.name}.Unit.{default_unit.name}), k_tolerancePercentage);
    }}
""".lstrip('\n')


class JavaTestWriter:
    package: str
    types: list[UnitType]
    equal_conversion_map: dict[UnitType, set[UnitType]]
    rec_graph: dict[UnitType, tuple[UnitType, int]]
    mul_graph: dict[UnitType, dict[UnitType, tuple[UnitType, int]]]
    div_graph: dict[UnitType, dict[UnitType, tuple[UnitType, int]]]
    tests: TestData
    lines: list[str]

    def __init__(self, package: str, types: list[UnitType], equal_conversion_map: dict[UnitType, set[UnitType]], rec_graph: dict[UnitType, UnitType], mul_graph: dict[UnitType, dict[UnitType, UnitType]], div_graph: dict[UnitType, dict[UnitType, UnitType]], tests: dict[str, list[str]]):
        self.package = package
        self.types = types
        self.equal_conversion_map = equal_conversion_map
        self.rec_graph = rec_graph
        self.mul_graph = mul_graph
        self.div_graph = div_graph
        self.tests = TestData(tests)
        self.lines = []

    def write_to(self, f: TextIO):
        self.lines = []
        self.add_header()
        for type in self.types:
            self.add_type(type)
        self.add_footer()

        f.writelines(self.lines)

    def add_type(self, type: UnitType):
        type_name = type.name

        default_unit = type.units[0]

        self.lines.append(get_identity_test(type))
        self.lines.append(get_string_test(type, default_unit))
        self.lines.append(get_equality_tests(type))
        self.lines.append(get_add_test(type, default_unit, self.tests.add))
        self.lines.append(get_subtract_test(type,default_unit, self.tests.subtract))
        self.lines.append(get_multiply_test(type,default_unit, self.tests.multiply))
        self.lines.append(get_divide_test(type,default_unit, self.tests.divide))
        self.lines.append(get_negate_test(type,default_unit, self.tests.negate))
        self.lines.append(get_absolute_value_test(type,default_unit, self.tests.absolute_value))
        self.lines.append(get_equal_to_test(type,default_unit, self.tests.equal_to))
        self.lines.append(get_not_equal_to_test(type,default_unit, self.tests.not_equal_to))
        self.lines.append(get_greater_than_test(type,default_unit, self.tests.greater_than))
        self.lines.append(get_greater_than_or_equal_to_test(type,default_unit, self.tests.greater_than_or_equal_to))
        self.lines.append(get_less_than_test(type,default_unit, self.tests.less_than))
        self.lines.append(get_less_than_or_equal_to_test(type,default_unit, self.tests.less_than_or_equal_to))
        self.lines.append(get_round_test(type,default_unit, self.tests.round))
        self.lines.append(get_ceil_test(type,default_unit, self.tests.ceil))
        self.lines.append(get_floor_test(type,default_unit, self.tests.floor))

    def add_header(self):
        self.lines.append(get_header(self.package))

    def add_footer(self):
        self.lines.append(get_footer())
