# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from JavaLibWriter import JavaLibWriter
from JavaTestWriter import JavaTestWriter
from Types import UnitType, Dimensionality
import argparse
import itertools
import json
import os
import warnings


def load(json_file) -> tuple[list[UnitType], dict[UnitType, set[UnitType]], dict[str, list[str]]]:
    with open(json_file, 'r', encoding='utf8') as f:
        data = json.load(f)

    types = [UnitType(t) for t in data['types']]
    tests = data['tests']

    # Convert relative dimensions to absolute
    change_made = True
    while change_made:
        change_made = False
        absolute_units : list[UnitType] = []
        relative_units : list[UnitType] = []
        [(relative_units if t.required_dims is None else absolute_units).append(t) for t in types]
        for r in relative_units:
            req = Dimensionality()
            opt = Dimensionality()
            for name, qty in r.relative_dims.dimensions.items():
                for a in absolute_units:
                    if a.name == name:
                        req += a.required_dims * qty
                        opt += a.optional_dims * qty
                        break
                else:
                    # No absolute unit matched name. This relative unit cannot be processed this frame.
                    break
            else:
                # No relative dimensions had issues, so dimensionality can be updated!
                r.required_dims = req
                r.optional_dims = opt
                r.relative_dims = None
                change_made = True

    # Ensure all relative types were resolved
    error_detected = False
    for t in types:
        if t.required_dims is None:
            print(f'Type {t.name} was unable to be resolved! Relative Dimensionality: {t.relative_dims}')
            error_detected = True
        if t.optional_dims is None:
            print(f'Internal error occurred while interpreting {t.name}! optional_dimensionality was unfilled.')
            error_detected = True
    assert not error_detected, 'Error detected while resolving relative dimensionalities. See printout for details.'

    # Ensure no two types are perfectly identical
    for a, b in itertools.combinations(types, 2):
        assert a.required_dims != b.required_dims or a.optional_dims != b.optional_dims, f'Types {a.name} and {b.name} are perfectly identical, which is not allowed.'

    equal_conversion_map = {t: set() for t in types}
    for a, b in itertools.combinations(types, 2):
        if a.required_dims == b.required_dims:
            equal_conversion_map[a].add(b)
            equal_conversion_map[b].add(a)

    return types, equal_conversion_map, tests

def get_closest_unit(req: Dimensionality, opt: Dimensionality, types: list[UnitType], description: str) -> tuple[UnitType, int]:
    lowest_cost = None
    best_result = []
    for res in types:
        if req != res.required_dims:
            # Operation invalid
            continue
        cost = opt.compare_to(res.optional_dims)
        if lowest_cost is None or cost < lowest_cost:
            lowest_cost = cost
            best_result = [res]
        elif cost == lowest_cost:
            best_result.append(res)
    if lowest_cost is None:
        # No valid results
        return None, None
    if len(best_result) == 1:
        # Provide the singular best result found
        return best_result[0], lowest_cost
    best_result.sort(key=lambda t: t.name)
    warnings.warn(f'{description} could equally result in any of the following: '
                  + ', '.join([r.name for r in best_result])
                  + '. To avoid randomly-changing results between versions, the first type in that list (alphabetically) will be used: '
                  + best_result[0].name)
    return best_result[0], lowest_cost

def make_1d_graph(types: list[UnitType], func) -> dict[UnitType, tuple[UnitType, int]]:
    graph = {}
    for lhs in types:
        result, cost = func(lhs, types)
        if result is not None:
            graph[lhs] = (result, cost)
    return graph

def make_2d_graph(types: list[UnitType], func) -> dict[UnitType, dict[UnitType, tuple[UnitType, int]]]:
    graph = {}
    for lhs in types:
        lhs_graph = {}
        for rhs in types:
            result, cost = func(lhs, rhs, types)
            if result is not None:
                lhs_graph[rhs] = (result, cost)
        graph[lhs] = lhs_graph
    return graph

def get_rec_result(lhs: UnitType, types: list[UnitType]) -> tuple[UnitType, int]:
    return get_closest_unit(-lhs.required_dims, -lhs.optional_dims, types, f'Reciprocating {lhs.name}')

def get_mul_result(lhs: UnitType, rhs: UnitType, types: list[UnitType]) -> tuple[UnitType, int]:
    return get_closest_unit(lhs.required_dims + rhs.required_dims, lhs.optional_dims + rhs.optional_dims, types, f'Multiplying {lhs.name} by {rhs.name}')

def get_div_result(lhs: UnitType, rhs: UnitType, types: list[UnitType]) -> tuple[UnitType, int]:
    return get_closest_unit(lhs.required_dims - rhs.required_dims, lhs.optional_dims - rhs.optional_dims, types, f'Dividing {lhs.name} by {rhs.name}')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                    prog='PythonGeneration',
                    description='Generated gstems-measurements using Python 3')
    parser.add_argument('-g', '--generation_dir')
    parser.add_argument('-j', '--json_file')
    parser.add_argument('-p', '--package')
    args = parser.parse_args()

    generation_dir = args.generation_dir.rstrip('/')
    package = args.package
    package_path = package.replace('.', '/')
    main_dir = f'{generation_dir}/main/java/{package_path}'
    test_dir = f'{generation_dir}/test/java/{package_path}'

    os.makedirs(main_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)

    [types, equal_conversion_map, tests] = load(args.json_file)

    rec_graph = make_1d_graph(types, get_rec_result)
    mul_graph = make_2d_graph(types, get_mul_result)
    div_graph = make_2d_graph(types, get_div_result)
    writer = JavaLibWriter(package, types, equal_conversion_map, rec_graph, mul_graph, div_graph, tests)
    with open(f'{main_dir}/Measurements.java', 'w', encoding='utf8') as f:
        writer.write_to(f)

    writer = JavaTestWriter(package, types, equal_conversion_map, rec_graph, mul_graph, div_graph, tests)
    with open(f'{test_dir}/Base_Test.java', 'w', encoding='utf8') as f:
        writer.write_to(f)
