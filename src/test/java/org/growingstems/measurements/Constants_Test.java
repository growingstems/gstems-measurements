/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;

import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Test;

public class Constants_Test {
    private static final double k_tolerancePercentage = 0.1;

    @Test
    public void speedOfLight() {
        assertPercentError(
                Velocity.SPEED_OF_LIGHT.getValue(Velocity.Unit.MILES_PER_HOUR),
                670616629.0,
                k_tolerancePercentage,
                "Speed of Light, MPH");
    }

    @Test
    public void gravity() {
        assertPercentError(
                Acceleration.EARTH_GRAVITY.getValue(Acceleration.Unit.FEET_PER_SECOND_SQUARED),
                32.17405,
                k_tolerancePercentage,
                "Gravity, ft/s^2");
    }

    @Test
    public void angles() {
        assertPercentError(
                Angle.TWO_PI.getValue(Angle.Unit.RADIANS),
                Angle.PI.mul(2.0).getValue(Angle.Unit.RADIANS),
                k_tolerancePercentage,
                "TWO_PI = 2 * PI");

        assertPercentError(
                Angle.PI.getValue(Angle.Unit.RADIANS),
                Angle.PI_BY_TWO.mul(2.0).getValue(Angle.Unit.RADIANS),
                k_tolerancePercentage,
                "PI = 2 * PI_BY_TWO");

        assertPercentError(
                Angle.TWO_PI.getValue(Angle.Unit.RADIANS),
                Angle.TAU.getValue(Angle.Unit.RADIANS),
                k_tolerancePercentage,
                "TWO_PI = TAU");
    }
}
