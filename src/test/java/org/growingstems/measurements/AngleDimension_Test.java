/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;

import org.growingstems.measurements.Measurements.AngularAcceleration;
import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.FrequencySquared;
import org.growingstems.measurements.Measurements.Mass;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Unitless;
import org.junit.jupiter.api.Test;

public class AngleDimension_Test {
    @Test
    public void angleAndAngularVelocity() {
        Angle angle = Angle.PI;
        Time time = Time.seconds(2.0);

        // This compiling verifies that Angle / Time results in AngularVelocity instead of Frequency
        AngularVelocity divisionResult = angle.div(time);
        AngularVelocity expectedResult = AngularVelocity.radiansPerSecond(Math.PI / 2.0);
        assertPercentError(expectedResult, divisionResult, 1e-6);

        // This compiling verifies that AngularVelocity * Time results in Angle instead of Unitless
        Angle multiplicationResult = divisionResult.mul(time);
        assertPercentError(angle, multiplicationResult, 1e-6);
    }

    @Test
    public void unitlessAndFrequency() {
        Unitless unitless = Unitless.PI;
        Time time = Time.seconds(2.0);

        // This compiling verifies that Unitless / Time results in Frequency instead of AngularVelocity
        Frequency divisionResult = unitless.div(time);
        Frequency expectedResult = Frequency.hertz(Math.PI / 2.0);
        assertPercentError(expectedResult, divisionResult, 1e-6);

        // This compiling verifies that Frequency * Time results in Unitless instead of Angle
        Unitless multiplicationResult = divisionResult.mul(time);
        assertPercentError(unitless, multiplicationResult, 1e-6);
    }

    @Test
    public void angularVelocityAndAngularAcceleration() {
        AngularVelocity angularVelocity = Angle.PI.div(Time.seconds(2.0));
        Time time = Time.seconds(2.0);

        // This compiling verifies that AngularVelocity / Time results in AngularAcceleration instead of
        // FrequencySquared
        AngularAcceleration divisionResult = angularVelocity.div(time);
        AngularAcceleration expectedResult = AngularAcceleration.radiansPerSecondSquared(Math.PI / 4.0);
        assertPercentError(expectedResult, divisionResult, 1e-6);

        // This compiling verifies that AngularAcceleration * Time results in AngularVelocity instead of
        // Unitless
        AngularVelocity multiplicationResult = divisionResult.mul(time);
        assertPercentError(angularVelocity, multiplicationResult, 1e-6);
    }

    @Test
    public void frequencyAndFrequencySquared() {
        Frequency frequency = Frequency.hertz(Math.PI);
        Time time = Time.seconds(2.0);

        // This compiling verifies that Frequency / Time results in FrequencySquared instead of
        // AngularAcceleration
        FrequencySquared divisionResult = frequency.div(time);
        FrequencySquared expectedResult = FrequencySquared.hertzSquared(Math.PI / 2.0);
        assertPercentError(expectedResult, divisionResult, 1e-6);

        // This compiling verifies that FrequencySquared * Time results in Frequency instead of
        // AngularVelocity
        Frequency multiplicationResult = divisionResult.mul(time);
        assertPercentError(frequency, multiplicationResult, 1e-6);
    }

    @Test
    public void angleAndAngleInteraction() {
        Angle a = Angle.PI;
        Angle b = Angle.TWO_PI;

        // This compiling verifies that Angle / Angle results in Unitless instead of Angle
        Unitless divisionResult = a.div(b);
        Unitless expectedDivisionResult = Unitless.none(0.5);
        assertPercentError(expectedDivisionResult, divisionResult, 1e-6);

        // This compiling verifies that Angle * Angle results in Angle instead of Unitless
        // NOTE: If a solid-angle unit (Angle squared) is ever added, this test *Should* stop working
        // and will require updates!
        // This is because this is testing a fallback that occurs when we're missing a unit. It should
        // be updated to use another unit that would invoke this fallback.
        Angle multiplicationResult = a.mul(b);
        Angle expectedMultiplicationResult = Angle.radians(Math.PI * Math.PI * 2.0);
        assertPercentError(expectedMultiplicationResult, multiplicationResult, 1e-6);
    }

    @Test
    public void angleFallback() {

        // Verify that Angles still behave as Unitless when no angle conversion is available
        // NOTE: If a angle-mass unit (Angle * Mass) is ever added, this test *Should* stop working and
        // will require updates!
        // This is because this is testing a fallback that occurs when we're missing a unit. It should
        // be updated to use another unit that would invoke this fallback.
        Angle a = Angle.radians(3.0);
        Mass b = a.mul(Mass.kilograms(20.0));
        Mass expectedResult = Mass.kilograms(60.0);
        assertPercentError(expectedResult, b, 1e-6);
    }
}
