/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.stream.Stream;
import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.Frequency;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Unitless;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class Unit_Test {
    private static final double k_tolerancePercentage = 1e-5;
    private static final double k_genericValue = 2.0;

    protected static Stream<Arguments> twoValueValues() {
        return Stream.of(
                Arguments.of(0.0, 0.0),
                Arguments.of(1.0, 0.0),
                Arguments.of(0.0, 1.0),
                Arguments.of(-1.0, 0.0),
                Arguments.of(0.0, -1.0),
                Arguments.of(0.213, 0.645),
                Arguments.of(0.17, 0.135),
                Arguments.of(-28.5, 20.4),
                Arguments.of(81.5, -57.4),
                Arguments.of(2341.23, -457.4),
                Arguments.of(43.5, 2980.3));
    }

    protected static Stream<Arguments> extremeValues() {
        return Stream.of(
                Arguments.of(Double.POSITIVE_INFINITY),
                Arguments.of(Double.NEGATIVE_INFINITY),
                Arguments.of(Double.NaN),
                Arguments.of(0.0),
                Arguments.of(1.0),
                Arguments.of(-1.0));
    }

    protected static Stream<Arguments> types() {
        Class<?>[] measurementClasses = Measurements.class.getClasses();
        Stream<Class<?>> customUnits = Stream.of(Angle.class);
        Stream<Class<?>> toFilter = Stream.concat(Stream.of(measurementClasses), customUnits);
        return toFilter
                .map(c -> {
                    try {
                        return c.getConstructor(double.class);
                    } catch (NoSuchMethodException e) {
                        return null;
                    }
                })
                .filter(con -> con != null)
                .map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("twoValueValues")
    public void compare(double a, double b) {
        Length lA = Length.inches(a);
        Length lB = Length.inches(b);
        int expected = Double.compare(a, b);
        int staticAnswer = Unit.compare(lA, lB);
        int nonStaticAnswer = lA.compareTo(lB);
        assertEquals(Integer.signum(expected), Integer.signum(staticAnswer));
        assertEquals(Integer.signum(expected), Integer.signum(nonStaticAnswer));
    }

    @ParameterizedTest
    @MethodSource("twoValueValues")
    public void max(double a, double b) {
        Acceleration accA = Acceleration.inchesPerSecondSquared(a);
        Acceleration accB = Acceleration.inchesPerSecondSquared(b);
        double expected = Double.max(a, b);
        double staticAnswer = Unit.max(accA, accB).asInchesPerSecondSquared();
        assertPercentError(expected, staticAnswer, k_tolerancePercentage);
    }

    @ParameterizedTest
    @MethodSource("twoValueValues")
    public void min(double a, double b) {
        Frequency fA = Frequency.gigahertz(a);
        Frequency fB = Frequency.gigahertz(b);
        double expected = Double.min(a, b);
        double staticAnswer = Unit.min(fA, fB).asGigahertz();
        assertPercentError(expected, staticAnswer, k_tolerancePercentage);
    }

    @ParameterizedTest
    @MethodSource("twoValueValues")
    public void sum(double a, double b) {
        Time tA = Time.milliseconds(a);
        Time lB = Time.milliseconds(b);
        double expected = Double.sum(a, b);
        double staticAnswer = Unit.sum(tA, lB).asMilliseconds();
        assertPercentError(expected, staticAnswer, k_tolerancePercentage);
    }

    @ParameterizedTest
    @MethodSource("extremeValues")
    public void isFinite(double a) {
        Angle angle = Angle.degrees(a);
        assertEquals(Double.isFinite(a), angle.isFinite());
        assertEquals(Double.isFinite(a), Unit.isFinite(angle));
    }

    @ParameterizedTest
    @MethodSource("extremeValues")
    public void isInfinite(double a) {
        Unitless unitless = Unitless.none(a);
        assertEquals(Double.isInfinite(a), unitless.isInfinite());
        assertEquals(Double.isInfinite(a), Unit.isInfinite(unitless));
    }

    @ParameterizedTest
    @MethodSource("extremeValues")
    public void isNaN(double a) {
        Velocity velocity = Velocity.feetPerSecond(a);
        assertEquals(Double.isNaN(a), velocity.isNaN());
        assertEquals(Double.isNaN(a), Unit.isNaN(velocity));
    }

    // The generic test functions must exist to validate return types.
    // Several of these tests mostly exist to verify the return types work properly - meaning a
    // compiling test is a valid test.
    // However, the results are still validated either way.

    public <U extends Unit<U>> void testGetBaseValue(Unit<U> u) {
        assertPercentError(k_genericValue, u.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testAddOther(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        U result = input.add(input);
        assertPercentError(2.0 * k_genericValue, result.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testSubOther(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        U result = input.sub(input);
        assertPercentError(0.0, result.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testMulDouble(Unit<U> u) {
        U result = u.mul(2.0);
        assertPercentError(2.0 * k_genericValue, result.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testDivDouble(Unit<U> u) {
        U result = u.div(2.0);
        assertPercentError(k_genericValue / 2.0, result.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testDivOther(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        Unitless result = input.div(input);
        assertPercentError(1.0, result.asNone(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testAbs(Unit<U> u) {
        U negative = u.neg();
        assertPercentError(k_genericValue, negative.abs().getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testNeg(Unit<U> u) {
        U negative = u.neg();
        assertPercentError(-k_genericValue, negative.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testPow(Unit<U> u) {
        U result = u.pow(2.0);
        assertPercentError(
                k_genericValue * k_genericValue, result.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testSqrt(Unit<U> u) {
        U result = u.sqrt();
        assertPercentError(Math.sqrt(k_genericValue), result.getBaseValue(), k_tolerancePercentage);
    }

    public <U extends Unit<U>> void testEq(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertTrue(input.eq(input));
    }

    public <U extends Unit<U>> void testNe(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertFalse(input.ne(input));
    }

    public <U extends Unit<U>> void testGt(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertFalse(input.gt(input));
    }

    public <U extends Unit<U>> void testGe(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertTrue(input.ge(input));
    }

    public <U extends Unit<U>> void testLt(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertFalse(input.lt(input));
    }

    public <U extends Unit<U>> void testLe(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertTrue(input.le(input));
    }

    public <U extends Unit<U>> void testCompareTo(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        U greater = input.abs().mul(2.0);
        assertEquals(0, input.compareTo(input));
        assertEquals(0, greater.compareTo(greater));
        assertEquals(1, greater.compareTo(input));
        assertEquals(-1, input.compareTo(greater));
        assertEquals(0, Unit.compare(input, input));
        assertEquals(0, Unit.compare(greater, greater));
        assertEquals(1, Unit.compare(greater, input));
        assertEquals(-1, Unit.compare(input, greater));
    }

    public <U extends Unit<U>> void testIsFinite(Unit<U> u) {
        assertTrue(u.isFinite());
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertTrue(Unit.isFinite(input));
    }

    public <U extends Unit<U>> void testIsInfinite(Unit<U> u) {
        assertFalse(u.isInfinite());
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertFalse(Unit.isInfinite(input));
    }

    public <U extends Unit<U>> void testIsNaN(Unit<U> u) {
        assertFalse(u.isNaN());
        @SuppressWarnings("unchecked")
        U input = (U) u;
        assertFalse(Unit.isNaN(input));
    }

    public <U extends Unit<U>> void testMax(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        U greater = input.abs().mul(2.0);
        U result = Unit.max(input, greater);
        assertTrue(result.eq(greater));
        result = Unit.max(greater, input);
        assertTrue(result.eq(greater));
    }

    public <U extends Unit<U>> void testMin(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        U greater = input.abs().mul(2.0);
        U result = Unit.min(input, greater);
        assertTrue(result.eq(input));
        result = Unit.min(greater, input);
        assertTrue(result.eq(input));
    }

    public <U extends Unit<U>> void testSum(Unit<U> u) {
        @SuppressWarnings("unchecked")
        U input = (U) u;
        U greater = input.abs().mul(2.0);
        U result = Unit.sum(input, greater);
        assertPercentError(
                input.add(greater).getBaseValue(), result.getBaseValue(), k_tolerancePercentage);
        result = Unit.sum(greater, input);
        assertPercentError(
                input.add(greater).getBaseValue(), result.getBaseValue(), k_tolerancePercentage);
    }

    @ParameterizedTest
    @MethodSource("types")
    public void unitGenericTests(Constructor<?> type)
            throws InstantiationException, IllegalAccessException, IllegalArgumentException,
                    InvocationTargetException {
        Object unitObject = type.newInstance(k_genericValue);
        assertTrue(unitObject instanceof Unit);
        Unit<?> unit = (Unit<?>) unitObject;
        testGetBaseValue(unit);
        testAddOther(unit);
        testSubOther(unit);
        testMulDouble(unit);
        testDivDouble(unit);
        testDivOther(unit);
        testAbs(unit);
        testNeg(unit);
        testPow(unit);
        testSqrt(unit);
        testEq(unit);
        testNe(unit);
        testGt(unit);
        testGe(unit);
        testLt(unit);
        testLe(unit);
        testCompareTo(unit);
        testIsFinite(unit);
        testIsInfinite(unit);
        testIsNaN(unit);
        testMax(unit);
        testMin(unit);
        testSum(unit);
    }
}
