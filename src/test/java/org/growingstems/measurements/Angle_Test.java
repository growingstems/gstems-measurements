/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import org.growingstems.measurements.Measurements.Unitless;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Angle_Test {
    private static final double k_tolerancePercentage = 0.1;

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "3.14159, 180.0, 0.5",
        "6.28318, 360.0, 1.0",
        "-6.28318, -360.0, -1.0",
        "-10.0, -572.96, -1.592"
    })
    public void conversions(double radians, double degrees, double revolutions) {
        List<Angle> angles = Arrays.asList(
                Angle.radians(radians), Angle.degrees(degrees), Angle.revolutions(revolutions));

        int i = 0;
        for (Angle angle : angles) {
            assertPercentError(
                    angle.getValue(Angle.Unit.RADIANS), radians, k_tolerancePercentage, "i: " + i);
            assertPercentError(
                    angle.getValue(Angle.Unit.DEGREES), degrees, k_tolerancePercentage, "i: " + i);
            assertPercentError(
                    angle.getValue(Angle.Unit.REVOLUTIONS), revolutions, k_tolerancePercentage, "i: " + i);
            i++;
        }
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 100.0, 0.0, 0.0",
        "3.14159, 10.0, 31.4159, 1800.0",
        "3.14159, -10.0, -31.4159, -1800.0",
        "-3.14159, 10.0, -31.4159, -1800.0",
        "-3.14159, -10.0, 31.4159, 1800.0"
    })
    public void mul(
            double initialRadians, double scaleFactor, double afterRadians, double afterDegrees) {
        Angle angleOne = Angle.radians(initialRadians);

        Angle angleTwo = angleOne.mul(scaleFactor);
        assertPercentError(angleTwo.getValue(Angle.Unit.RADIANS), afterRadians, k_tolerancePercentage);
        assertPercentError(angleTwo.getValue(Angle.Unit.DEGREES), afterDegrees, k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "3.14159, -3.14159, -180.0",
        "-3.14159, 3.14159, 180.0",
        "10.0, -10.0, -572.96"
    })
    public void inverse(double initialRadians, double afterRadians, double afterDegrees) {
        Angle angleOne = Angle.radians(initialRadians);

        Angle angleTwo = angleOne.mul(-1);
        assertPercentError(afterRadians, angleTwo.getValue(Angle.Unit.RADIANS), k_tolerancePercentage);
        assertPercentError(afterDegrees, angleTwo.getValue(Angle.Unit.DEGREES), k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "1.0, 0.0, 1.0",
        "0.0, 0.0, 0.0",
        "0.0, 1.0, 1.0",
        "0.0, 0.0, 0.0",
        "-1.0, 0.0, -1.0",
        "0.0, 0.0, 0.0",
        "0.213, 0.645, 0.858",
        "0.17, 0.135, 0.305",
        "-28.5, 20.4, -8.1",
        "81.5, -57.4, 24.1",
        "2341.23, -457.4, 1883.83",
        "43.5, 2980.3, 3023.8"
    })
    public void add(double a_rad, double b_rad, double expected_rad) {
        Angle a = Angle.radians(a_rad);
        Angle b = Angle.radians(b_rad);

        Angle result = a.add(b);

        assertPercentError(expected_rad, result.getValue(Angle.Unit.RADIANS), k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "1.0, 0.0, 1.0",
        "0.0, 1.0, -1.0",
        "-1.0, 0.0, -1.0",
        "0.213, 0.645, -0.432",
        "0.17, 0.135, 0.035",
        "-28.5, -20.4, -8.1",
        "81.5, 57.4, 24.1",
        "2341.23, 457.4, 1883.83",
        "43.5, -2980.3, 3023.8"
    })
    public void subtract(double a_rad, double b_rad, double expected_rad) {
        Angle a = Angle.radians(a_rad);
        Angle b = Angle.radians(b_rad);

        Angle result = a.sub(b);

        assertPercentError(expected_rad, result.getValue(Angle.Unit.RADIANS), k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "0.25, 0.0, 0.25",
        "0.0, 0.25, -0.25",
        "-0.25, 0.0, -0.25",
        "0.0, -0.25, 0.25",
        "0.75, 0.0, -0.25",
        "0.0, 0.75, 0.25",
        "-0.75, 0.0, 0.25",
        "0.0, -0.75, -0.25",
        "1.0, 0.0, 0.0",
        "0.0, 1.0, 0.0",
        "-1.0, 0.0, 0.0",
        "0.0, -1.0, 0.0",
        "0.213, 0.645, -0.432",
        "0.17, 0.135, 0.035",
        "-28.5, -20.4, -0.1",
        "81.5, 57.4, 0.1",
        "2341.23, 457.4, -0.17",
        "43.5, -2980.3, -0.2"
    })
    public void difference(double a_rot, double b_rot, double expected_rot) {
        Angle a = Angle.revolutions(a_rot);
        Angle b = Angle.revolutions(b_rot);

        Angle resultOne = a.difference(b);
        Angle resultTwo = b.difference(a);

        assertPercentError(
                expected_rot, resultOne.getValue(Angle.Unit.REVOLUTIONS), k_tolerancePercentage);
        assertPercentError(
                -expected_rot, resultTwo.getValue(Angle.Unit.REVOLUTIONS), k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0",
        "1.0, 1.0",
        "100.0, 100.0",
        "10000.0, 10000.0",
        "-1.0, 1.0",
        "-100.0, 100.0",
        "-10000.0, 10000.0"
    })
    public void absoluteValue(double before_deg, double after_deg) {
        Angle angle = Angle.degrees(before_deg);
        // Verify abs
        assertPercentError(after_deg, angle.abs().getValue(Angle.Unit.DEGREES), k_tolerancePercentage);
        // Verify angle is unchanged
        assertPercentError(before_deg, angle.getValue(Angle.Unit.DEGREES), k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 1.0, 0.0",
        "30.0, 0.5, 0.86603, 0.57735",
        "135.0, 0.70711, -0.70711, -1.0",
        "-7000.0, -0.34202, -0.93969, 0.36397"
    })
    public void trigonometry(double angle_deg, double sine, double cosine, double tangent) {
        Angle a = Angle.degrees(angle_deg);
        assertPercentError(a.sin(), sine, k_tolerancePercentage);
        assertPercentError(a.cos(), cosine, k_tolerancePercentage);
        assertPercentError(a.tan(), tangent, k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 90.0, 0.0",
        "1.0, 90.0, 0.0, 45.0",
        "-1.0, -90.0, 180.0, -45.0",
        "-0.5, -30.0, 120.0, -26.565"
    })
    public void inverseTrigonometry(double input, double asin_deg, double acos_deg, double atan_deg) {
        Angle aSin = Angle.asin(input);
        Angle aCos = Angle.acos(input);
        Angle aTan = Angle.atan(input);
        assertPercentError(aSin.getValue(Angle.Unit.DEGREES), asin_deg, k_tolerancePercentage);
        assertPercentError(aCos.getValue(Angle.Unit.DEGREES), acos_deg, k_tolerancePercentage);
        assertPercentError(aTan.getValue(Angle.Unit.DEGREES), atan_deg, k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 1.0, 90.0",
        "0.0, -1.0, -90.0",
        "1.0, 0.0, 0.0",
        "-1.0, 0.0, 180.0",
        "100.0, 100.0, 45.0",
        "50.0, 25.0, 26.565",
        "50.0, -25.0, -26.565",
        "-50.0, 25.0, 153.435",
        "-50.0, -25.0, -153.435"
    })
    // (0.0, 0.0) is NOT TESTED because Java does not define the result.
    public void atan2(double x, double y, double atan2_deg) {
        Angle result = Angle.atan2(y, x);
        assertPercentError(result.getValue(Angle.Unit.DEGREES), atan2_deg, k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "0.0, 179.9, 0.0",
        "0.0, 180.0, 360.0",
        "-0.1, 180.0, 359.9",
        "0.0, 180.1, 360.0",
        "-180.0, 180.0, 180.0",
        "-179.0, 179.0, 181.0",
        "90.0, -270.0, -270.0",
        "-90.0, -270.0, -90.0",
    })
    public void shiftWrappedAngleScopeTest(
            double wrapAng_deg, double reference_deg, double expected_deg) {
        Angle angle = Angle.degrees(wrapAng_deg);
        assertEquals(
                expected_deg,
                angle.shiftWrappedAngleScope(Angle.degrees(reference_deg)).getValue(Angle.Unit.DEGREES),
                1E-6,
                "Test");
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0, 0.0, 0.0",
        "-80.0, 350.0, -180.0, 360.0, 280.0",
        "-90.0, 350.0, -180.0, 270.0, 270.0",
        "-90.0, -45.0, -90.0, 270.0, -90.0",
    })
    public void shiftWrappedAngleScopeExtendedTest(
            double boundedAng_deg,
            double reference_deg,
            double low_deg,
            double high_deg,
            double expected_deg) {
        Angle bounded = Angle.degrees(boundedAng_deg);
        Angle reference = Angle.degrees(reference_deg);
        Angle low = Angle.degrees(low_deg);
        Angle high = Angle.degrees(high_deg);
        Angle result = bounded.shiftWrappedAngleScope(reference, low, high);
        assertEquals(expected_deg, result.getValue(Angle.Unit.DEGREES), 1E-6);
    }

    @Test
    public void convertToUnitless() {
        Angle angle = Angle.radians(2.0);
        Unitless unitless = angle.asUnitless();
        assertEquals(angle.getBaseValue(), unitless.getBaseValue(), 1E-6);
    }
}
