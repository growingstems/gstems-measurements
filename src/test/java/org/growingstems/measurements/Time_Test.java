/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.measurements.Measurements.Time;
import static org.growingstems.utils.TestUtils.assertPercentError;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Time_Test {
    private static final double k_tolerancePercentage = 0.1;

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0, 0.0",
        "1.0, 1000.0, 1000000.0, 0.0166667",
        "0.001, 1.0, 1000.0, 1.66667e-5",
        "0.000001, 0.001, 1.0, 1.66666e-8",
        "60.0, 60000.0, 6e+7, 1.0"
    })
    public void conversions(
            double seconds, double milliseconds, double microseconds, double minutes) {
        List<Time> times = Arrays.asList(
                Time.seconds(seconds),
                Time.milliseconds(milliseconds),
                Time.microseconds(microseconds),
                Time.minutes(minutes));

        for (Time time : times) {
            assertPercentError(time.getValue(Time.Unit.SECONDS), seconds, k_tolerancePercentage);
            assertPercentError(
                    time.getValue(Time.Unit.MILLISECONDS), milliseconds, k_tolerancePercentage);
            assertPercentError(
                    time.getValue(Time.Unit.MICROSECONDS), microseconds, k_tolerancePercentage);
            assertPercentError(time.getValue(Time.Unit.MINUTES), minutes, k_tolerancePercentage);
        }
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 100.0, 0.0",
        "3.14159, 10.0, 31.4159",
        "3.14159, -10.0, -31.4159",
        "-3.14159, 10.0, -31.4159",
        "-3.14159, -10.0, 31.4159"
    })
    public void mul(double initialSeconds, double scaleFactor, double afterSeconds) {
        Time timeOne = Time.seconds(initialSeconds);

        Time timeTwo = timeOne.mul(scaleFactor);
        assertPercentError(timeTwo.getValue(Time.Unit.SECONDS), afterSeconds, k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({"0.0, 0.0", "3.14159, -3.14159", "-3.14159, 3.14159", "10.0, -10.0"})
    public void inverse(double initialSeconds, double afterSeconds) {
        Time timeOne = Time.seconds(initialSeconds);

        Time timeTwo = timeOne.mul(-1);
        assertPercentError(timeTwo.getValue(Time.Unit.SECONDS), afterSeconds, k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "1.0, 0.0, 1.0",
        "0.0, 0.0, 0.0",
        "0.0, 1.0, 1.0",
        "0.0, 0.0, 0.0",
        "-1.0, 0.0, -1.0",
        "0.0, 0.0, 0.0",
        "0.213, 0.645, 0.858",
        "0.17, 0.135, 0.305",
        "-28.5, 20.4, -8.1",
        "81.5, -57.4, 24.1",
        "2341.23, -457.4, 1883.83",
        "43.5, 2980.3, 3023.8"
    })
    public void add(double a_sec, double b_sec, double expected_sec) {
        Time a = Time.seconds(a_sec);
        Time b = Time.seconds(b_sec);

        Time result = a.add(b);

        assertPercentError(expected_sec, result.getValue(Time.Unit.SECONDS), k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0, 0.0",
        "1.0, 0.0, 1.0",
        "0.0, 1.0, -1.0",
        "-1.0, 0.0, -1.0",
        "0.213, 0.645, -0.432",
        "0.17, 0.135, 0.035",
        "-28.5, -20.4, -8.1",
        "81.5, 57.4, 24.1",
        "2341.23, 457.4, 1883.83",
        "43.5, -2980.3, 3023.8"
    })
    public void subtract(double a_sec, double b_sec, double expected_sec) {
        Time a = Time.seconds(a_sec);
        Time b = Time.seconds(b_sec);

        Time result = a.sub(b);

        assertPercentError(expected_sec, result.getValue(Time.Unit.SECONDS), k_tolerancePercentage);
    }

    @ParameterizedTest
    @CsvSource({
        "0.0, 0.0",
        "1.0, 1.0",
        "100.0, 100.0",
        "10000.0, 10000.0",
        "-1.0, 1.0",
        "-100.0, 100.0",
        "-10000.0, 10000.0"
    })
    public void absoluteValue(double before_sec, double after_sec) {
        Time time = Time.seconds(before_sec);
        // Verify abs
        assertPercentError(after_sec, time.abs().getValue(Time.Unit.SECONDS), k_tolerancePercentage);
        // Verify time is unchanged
        assertPercentError(before_sec, time.getValue(Time.Unit.SECONDS), k_tolerancePercentage);
    }
}
