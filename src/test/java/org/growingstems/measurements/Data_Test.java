/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;

import org.growingstems.measurements.Measurements.Data;
import org.growingstems.measurements.Measurements.DataRate;
import org.growingstems.measurements.Measurements.Time;
import org.junit.jupiter.api.Test;

public class Data_Test {
    private static final double k_tolerancePercentage = 0.1;

    @Test
    public void basicDataRate() {
        DataRate p = DataRate.kilobitsPerSecond(5.0);
        Time t = Time.seconds(6.0);
        Data e = Data.kilobits(30.0);
        assertPercentError(e, p.mul(t), k_tolerancePercentage);
    }
}
