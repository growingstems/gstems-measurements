/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ToString_Test {
    @Test
    public void radiansToString() {
        Angle twoPi = Angle.TWO_PI;
        String str = twoPi.toString(Angle.Unit.RADIANS);

        // Default unit is radians
        assertEquals(twoPi.toString(), str);

        // Don't over-specify the length of the string, but verify that it fits the expected format.
        var strs = str.split(" ");
        assertEquals(2, strs.length);
        assertTrue(strs[0].startsWith("6.28"), strs[0]);
        assertEquals(strs[1].toLowerCase(), "radians", strs[1]);
    }

    @Test
    public void degreesToString() {
        Angle twoPi = Angle.TWO_PI;
        String str = twoPi.toString(Angle.Unit.DEGREES);

        // Don't over-specify the length of the string, but verify that it fits the expected format.
        var strs = str.split(" ");
        assertEquals(2, strs.length);
        assertTrue(strs[0].startsWith("360.0"), strs[0]);
        assertEquals(strs[1].toLowerCase(), "degrees", strs[1]);
    }

    @Test
    public void revolutionsToString() {
        Angle twoPi = Angle.TWO_PI;
        String str = twoPi.toString(Angle.Unit.REVOLUTIONS);

        // Don't over-specify the length of the string, but verify that it fits the expected format.
        var strs = str.split(" ");
        assertEquals(2, strs.length);
        assertTrue(strs[0].startsWith("1.0"), strs[0]);
        assertEquals(strs[1].toLowerCase(), "revolutions", strs[1]);
    }
}
