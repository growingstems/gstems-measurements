/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;

import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.AngularAcceleration;
import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Current;
import org.growingstems.measurements.Measurements.Power;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Measurements.VoltagePerAcceleration;
import org.growingstems.measurements.Measurements.VoltagePerFrequency;
import org.growingstems.measurements.Measurements.VoltagePerFrequencySquared;
import org.growingstems.measurements.Measurements.VoltagePerTimeCubed;
import org.growingstems.measurements.Measurements.VoltagePerTimeSquared;
import org.growingstems.measurements.Measurements.VoltagePerVelocity;
import org.growingstems.measurements.Measurements.VoltageRate;
import org.junit.jupiter.api.Test;

public class Volt_Test {
    private static final double k_tolerancePercentage = 0.1;

    @Test
    public void basicElectrical() {
        Voltage v = Voltage.volts(10.0);
        Current c = Current.amps(20.0);
        Power p = Power.watts(200.0);
        assertPercentError(p.asWatts(), v.mul(c).asWatts(), k_tolerancePercentage);
    }

    @Test
    public void voltageModel() {
        VoltagePerAcceleration drive_kA =
                Voltage.volts(8.0).div(Acceleration.inchesPerSecondSquared(2.0));
        VoltagePerVelocity drive_kV = Voltage.volts(10.0).div(Velocity.inchesPerSecond(12.0));

        Acceleration desiredDriveAcceleration = Acceleration.inchesPerSecondSquared(5.0);
        Velocity desiredDriveVelocity = Velocity.inchesPerSecond(24.0);
        Voltage requiredDriveVoltage = Voltage.volts(40.0);

        assertPercentError(
                requiredDriveVoltage.asVolts(),
                drive_kA
                        .mul(desiredDriveAcceleration)
                        .add(drive_kV.mul(desiredDriveVelocity))
                        .asVolts(),
                k_tolerancePercentage);

        VoltagePerFrequencySquared turn_kA =
                Voltage.volts(8.0).div(AngularAcceleration.degreesPerSecondSquared(10.0));
        VoltagePerFrequency turn_kV = Voltage.volts(10.0).div(AngularVelocity.degreesPerSecond(20.0));

        AngularAcceleration desiredTurnAcceleration = AngularAcceleration.degreesPerSecondSquared(2.0);
        AngularVelocity desiredTurnVelocity = AngularVelocity.degreesPerSecond(40.0);
        Voltage requiredTurnVoltage = Voltage.volts(21.6);

        assertPercentError(
                requiredTurnVoltage.asVolts(),
                turn_kA
                        .mul(desiredTurnAcceleration)
                        .add(turn_kV.mul(desiredTurnVelocity))
                        .asVolts(),
                k_tolerancePercentage);
    }

    public void voltageRates() {
        Voltage v = Voltage.volts(2.0);
        VoltageRate vps = VoltageRate.voltsPerSecond(4.0);
        VoltagePerTimeSquared vps2 = VoltagePerTimeSquared.voltsPerSecondSquared(8.0);
        VoltagePerTimeCubed vps3 = VoltagePerTimeCubed.voltsPerSecondCubed(16.0);
        Time dt = Time.seconds(0.5);
        assertPercentError(dt, v.div(vps), k_tolerancePercentage);
        assertPercentError(v, vps.mul(dt), k_tolerancePercentage);
        assertPercentError(vps, vps2.mul(dt), k_tolerancePercentage);
        assertPercentError(vps2, vps3.mul(dt), k_tolerancePercentage);
    }
}
