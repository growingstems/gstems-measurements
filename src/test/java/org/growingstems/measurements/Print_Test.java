package org.growingstems.measurements;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class Print_Test {
    @Test
    public void degreeStrings() {
        String degreesName = "DEGREES";
        String degreesPrettyName = "Degrees";
        String degreesAbbreviation = "deg";
        String degreesSymbol = "°";

        assertEquals(degreesName, Angle.Unit.DEGREES.getName());
        assertEquals(degreesPrettyName, Angle.Unit.DEGREES.getPrettyName());
        assertEquals(degreesAbbreviation, Angle.Unit.DEGREES.getAbbreviation());
        assertEquals(degreesSymbol, Angle.Unit.DEGREES.getSymbol());

        assertEquals(degreesName, Angle.Unit.DEGREES.toString(PrintStyle.STANDARD));
        assertEquals(degreesPrettyName, Angle.Unit.DEGREES.toString(PrintStyle.PRETTY));
        assertEquals(degreesAbbreviation, Angle.Unit.DEGREES.toString(PrintStyle.ABBREVIATION));
        assertEquals(degreesSymbol, Angle.Unit.DEGREES.toString(PrintStyle.SYMBOL));
    }

    @Test
    public void radianStrings() {
        String radiansName = "RADIANS";
        String radiansPrettyName = "Radians";
        String radiansAbbreviation = "rad";
        String radiansSymbol = "ᴿ";

        assertEquals(radiansName, Angle.Unit.RADIANS.getName());
        assertEquals(radiansPrettyName, Angle.Unit.RADIANS.getPrettyName());
        assertEquals(radiansAbbreviation, Angle.Unit.RADIANS.getAbbreviation());
        assertEquals(radiansSymbol, Angle.Unit.RADIANS.getSymbol());

        assertEquals(radiansName, Angle.Unit.RADIANS.toString(PrintStyle.STANDARD));
        assertEquals(radiansPrettyName, Angle.Unit.RADIANS.toString(PrintStyle.PRETTY));
        assertEquals(radiansAbbreviation, Angle.Unit.RADIANS.toString(PrintStyle.ABBREVIATION));
        assertEquals(radiansSymbol, Angle.Unit.RADIANS.toString(PrintStyle.SYMBOL));
    }

    @Test
    public void revolutionStrings() {
        String revolutionsName = "REVOLUTIONS";
        String revolutionsPrettyName = "Revolutions";
        String revolutionsAbbreviation = "rev";
        String revolutionsSymbol = "rev";

        assertEquals(revolutionsName, Angle.Unit.REVOLUTIONS.getName());
        assertEquals(revolutionsPrettyName, Angle.Unit.REVOLUTIONS.getPrettyName());
        assertEquals(revolutionsAbbreviation, Angle.Unit.REVOLUTIONS.getAbbreviation());
        assertEquals(revolutionsSymbol, Angle.Unit.REVOLUTIONS.getSymbol());

        assertEquals(revolutionsName, Angle.Unit.REVOLUTIONS.toString(PrintStyle.STANDARD));
        assertEquals(revolutionsPrettyName, Angle.Unit.REVOLUTIONS.toString(PrintStyle.PRETTY));
        assertEquals(revolutionsAbbreviation, Angle.Unit.REVOLUTIONS.toString(PrintStyle.ABBREVIATION));
        assertEquals(revolutionsSymbol, Angle.Unit.REVOLUTIONS.toString(PrintStyle.SYMBOL));
    }
}
