/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;

import org.growingstems.measurements.Measurements.Absement;
import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.AngularAbsement;
import org.growingstems.measurements.Measurements.AngularAcceleration;
import org.growingstems.measurements.Measurements.AngularJerk;
import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Jerk;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Test;

// These tests are primarily to show a successful build, so they use the SI base units to make the
// math easy
public class HighTimeDegree_Test {
    @Test
    public void angularTerms() {
        var t = Time.seconds(2.0);
        var i = AngularAbsement.radianSeconds(16.0);
        var p = Angle.radians(8.0);
        var v = AngularVelocity.radiansPerSecond(4.0);
        var a = AngularAcceleration.radiansPerSecondSquared(2.0);
        var j = AngularJerk.radiansPerSecondCubed(1.0);

        assertPercentError(a, j.mul(t), 0.1);
        assertPercentError(v, a.mul(t), 0.1);
        assertPercentError(p, v.mul(t), 0.1);
        assertPercentError(i, p.mul(t), 0.1);

        assertPercentError(j, a.div(t), 0.1);
        assertPercentError(a, v.div(t), 0.1);
        assertPercentError(v, p.div(t), 0.1);
        assertPercentError(p, i.div(t), 0.1);
    }

    @Test
    public void lengthTerms() {
        var t = Time.seconds(2.0);
        var i = Absement.meterSeconds(16.0);
        var p = Length.meters(8.0);
        var v = Velocity.metersPerSecond(4.0);
        var a = Acceleration.metersPerSecondSquared(2.0);
        var j = Jerk.metersPerSecondCubed(1.0);

        assertPercentError(a, j.mul(t), 0.1);
        assertPercentError(v, a.mul(t), 0.1);
        assertPercentError(p, v.mul(t), 0.1);
        assertPercentError(i, p.mul(t), 0.1);

        assertPercentError(j, a.div(t), 0.1);
        assertPercentError(a, v.div(t), 0.1);
        assertPercentError(v, p.div(t), 0.1);
        assertPercentError(p, i.div(t), 0.1);
    }
}
