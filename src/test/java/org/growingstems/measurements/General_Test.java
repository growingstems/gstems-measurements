/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.measurements.Measurements.Force;
import static org.growingstems.measurements.Measurements.Frequency;
import static org.growingstems.measurements.Measurements.Length;
import static org.growingstems.measurements.Measurements.Mass;
import static org.growingstems.measurements.Measurements.Time;
import static org.growingstems.measurements.Measurements.Volume;
import static org.growingstems.utils.TestUtils.assertPercentError;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Unitless;
import org.junit.jupiter.api.Test;

public class General_Test {
    @Test
    public void concatMul() {
        Length a = Length.meters(1.0);
        Length b = Length.meters(2.0);
        Length c = Length.meters(3.0);
        Volume v = a.mul(b).mul(c);
        assertPercentError(6.0, v.getValue(Volume.Unit.METERS_CUBED), 0.001);
    }

    @Test
    public void concatDiv() {
        Volume v = Volume.metersCubed(4.0);
        Length a = Length.meters(1.0);
        Length b = Length.meters(2.0);
        Length c = v.div(a).div(b);
        assertPercentError(2.0, c.getValue(Length.Unit.METERS), 0.001);
    }

    @Test
    public void concatMulDiv() {
        Mass m = Mass.grams(1.0);
        Length a = Length.meters(1.0);
        Time t = Time.seconds(1.0);
        Force f = m.mul(a.div(t).div(t));
        assertPercentError(0.001, f.getValue(Force.Unit.NEWTONS), 0.001);
    }

    @Test
    public void reciprocate() {
        Time t = Time.seconds(2.0);
        Frequency f = t.rec();
        Time t2 = f.rec();
        assertTrue(t.eq(t2));
        assertPercentError(0.5, f.getValue(Frequency.Unit.HERTZ), 0.001);
    }

    @Test
    public void alias() {
        AngularVelocity av = AngularVelocity.radiansPerSecond(5.0);
        Frequency f = av.asFrequency();
        assertPercentError(5.0, f.getValue(Frequency.Unit.HERTZ), 0.001);
        av = f.asAngularVelocity();
        assertPercentError(5.0, av.getValue(AngularVelocity.Unit.RADIANS_PER_SECOND), 0.001);

        Angle a = Angle.radians(3.0);
        Unitless u = a.asUnitless();
        assertPercentError(3.0, u.getValue(Unitless.Unit.NONE), 0.001);
        a = u.asAngle();
        assertPercentError(3.0, a.getValue(Angle.Unit.RADIANS), 0.001);
    }

    @Test
    public void reciprocateAlias() {
        Time t = Time.seconds(5.0);
        Frequency f = t.rec();
        assertPercentError(0.2, f.getValue(Frequency.Unit.HERTZ), 0.001);
    }
}
