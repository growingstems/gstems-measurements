/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;

import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Energy;
import org.growingstems.measurements.Measurements.Power;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Torque;
import org.junit.jupiter.api.Test;

public class TorqueEnergy_Test {
    private static final double k_tolerancePercentage = 0.1;

    @Test
    public void basicEnergy() {
        Power p = Power.kilowatts(5.0);
        Time t = Time.hours(6.0);
        Energy e = Energy.kilowattHours(30.0);
        assertPercentError(e, p.mul(t), k_tolerancePercentage);
    }

    @Test
    public void mechanicalWork() {
        // Mechanical work due to an applied torque
        // https://en.wikipedia.org/wiki/Torque
        var t = Torque.newtonMeters(5.0);
        var a = Angle.radians(2.0);
        var e = Energy.joules(10.0);
        assertPercentError(e, t.mul(a), k_tolerancePercentage);
    }

    @Test
    public void mechanicalPower() {
        // Mechanical power due to an applied torque
        // https://en.wikipedia.org/wiki/Torque
        var t = Torque.newtonMeters(5.0);
        var a = AngularVelocity.radiansPerSecond(2.0);
        var e = Power.watts(10.0);
        assertPercentError(e, t.mul(a), k_tolerancePercentage);
    }
}
