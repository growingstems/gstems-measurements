/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.measurements;

import static org.growingstems.utils.TestUtils.assertPercentError;

import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Test;

/** This is meant to provide a clear example of how to use the MulBy and DivBy interfaces */
public class MulBy_DivBy_Test {
    public static class TimeDerivative<In extends Unit<In> & DivByTime<Res>, Res extends Unit<Res>> {
        private Time m_prevTime;
        private In m_prevInput;

        public TimeDerivative(In initialInput, Time initialTime) {
            m_prevInput = initialInput;
            m_prevTime = initialTime;
        }

        public Res update(In input, Time time) {
            Res derivative = input.sub(m_prevInput).div(time.sub(m_prevTime));

            m_prevInput = input;
            m_prevTime = time;

            return derivative;
        }
    }

    @Test
    public void timeDerivativeTest() {
        TimeDerivative<Length, Velocity> deriver = new TimeDerivative<>(Length.ZERO, Time.ZERO);
        Velocity derivative = deriver.update(Length.feet(5.0), Time.seconds(30.0));
        assertPercentError(2.0, derivative.asInchesPerSecond(), 0.001);
    }
}
