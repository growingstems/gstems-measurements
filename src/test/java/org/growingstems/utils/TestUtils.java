/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.utils;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.growingstems.measurements.Unit;

public class TestUtils {
    public static double percentError(double expected, double actual) {
        if (expected == 0.0 && actual == 0.0) {
            return 0.0;
        } else if (expected == 0) {
            return Double.POSITIVE_INFINITY;
        }

        return Math.abs((actual - expected) / expected) * 100.0;
    }

    public static void assertPercentError(
            double expected, double actual, double percentError, String message) {
        double pErr = percentError(expected, actual);
        String usefulError = "Expected: " + expected + "\nActual: " + actual + "\nError: " + pErr
                + "%\nAllowed Error: " + percentError + "%";
        if (message != null) {
            usefulError += "\n" + message;
        }
        assertTrue(pErr <= percentError, usefulError);
    }

    public static void assertPercentError(double expected, double actual, double percentError) {
        assertPercentError(expected, actual, percentError, null);
    }

    public static <U extends Unit<U>> void assertPercentError(
            U expected, U actual, double percentError, String message) {
        assertPercentError(expected.getBaseValue(), actual.getBaseValue(), percentError, message);
    }

    public static <U extends Unit<U>> void assertPercentError(
            U expected, U actual, double percentError) {
        assertPercentError(expected.getBaseValue(), actual.getBaseValue(), percentError);
    }
}
